-- SQLite
-- SQLite
-- SQLite
DROP TABLE vacancy;
DROP TABLE company;
DROP TABLE company_vacancy;

DELETE FROM vacancy;
DELETE FROM company_vacancy;

CREATE TABLE vacancy (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name varchar(255) NOT NULL,
  price_from INTEGER,
  price_to INTEGER,
  region_id INTEGER,
  prof_area_id INTEGER,
  work_expirience varchar(255),
  work_schedule varchar(255),
  description varchar(255),
  timestamp TIMESTAMP
);

CREATE TABLE company (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  name varchar(255) NOT NULL
);

CREATE TABLE company_vacancy (
  id INTEGER PRIMARY KEY AUTOINCREMENT,
  company_id INTEGER NOT NULL,
  vacancy_id INTEGER NOT NULL
);

INSERT INTO company (name) VALUES ('hrEnergy');
INSERT INTO vacancy (name) VALUES ('Разработчик');

SELECT * FROM company;
SELECT * FROM vacancy;
SELECT * FROM company_vacancy;