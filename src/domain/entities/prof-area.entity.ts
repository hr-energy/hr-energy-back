export type TProfAreaId = number;

export class ProfArea {
  constructor(private readonly _id: TProfAreaId) {}

  get id() {
    return this._id;
  }
}
