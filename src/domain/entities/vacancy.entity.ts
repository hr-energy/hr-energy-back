import { CompanyToVacancyWindowEntity } from './company-to-vacancy-window.entity';
import { CompanyEntity, TCompanyId } from './company.entity';
import { TProfAreaId } from './prof-area.entity';
import { TRegionId } from './region.entity';
import { WorkExpirienceValue } from './work-expirience.entity';
import { WorkScheduleValue } from './work-schedule.entity';

export type TVacancyId = number;

export class VacancyEntity {
  constructor(
    private readonly _id: TVacancyId,
    private readonly _name: string,
    private readonly _priceFrom: number,
    private readonly _priceTo: number,
    private readonly _regionId: TRegionId,
    private readonly _profAreaId: TProfAreaId,
    private readonly _workExpirience: WorkExpirienceValue,
    private readonly _workSchedule: WorkScheduleValue,
    private readonly _timestamp: string,
    private readonly _description: string,
    private readonly _company?: CompanyEntity,
    private readonly _companyToVacancyWindow?: CompanyToVacancyWindowEntity,
  ) {}

  get id(): TVacancyId {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  public get timestamp(): string {
    return this._timestamp;
  }
  public get workSchedule(): WorkScheduleValue {
    return this._workSchedule;
  }
  public get workExpirience(): WorkExpirienceValue {
    return this._workExpirience;
  }
  public get profAreaId(): TProfAreaId {
    return this._profAreaId;
  }
  public get regionId(): TRegionId {
    return this._regionId;
  }
  public get priceTo(): number {
    return this._priceTo;
  }
  public get priceFrom(): number {
    return this._priceFrom;
  }

  public get description(): string {
    return this._description;
  }

  public get company(): CompanyEntity {
    return this._company;
  }

  public getCompanyId(): TCompanyId {
    return this._companyToVacancyWindow.getCompanyIdByVacancyId(this.id);
  }

  static of(vacancy: VacancyEntity) {
    const {
      id,
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      company,
      description,
      timestamp,
    } = vacancy;
    const entity = new VacancyEntity(
      id,
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      description,
      timestamp,
      company,
    );
    return {
      id: entity.id,
      name: entity.name,
      priceFrom: entity.priceFrom,
      priceTo: entity.priceTo,
      regionId: entity.regionId,
      profAreaId: entity.profAreaId,
      workExpirience: entity.workExpirience,
      workSchedule: entity.workSchedule,
      company: CompanyEntity.of(entity.company),
      description: entity.description,
      timestamp: entity.timestamp,
    };
  }
}
