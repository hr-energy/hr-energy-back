import { CompanyToVacancyEntity } from './company-to-vacancy.entity';
import { TCompanyId } from './company.entity';
import { TVacancyId } from './vacancy.entity';

export class CompanyToVacancyWindowEntity {
  private readonly _companyToVacancy: CompanyToVacancyEntity[];

  get companyToVacancy(): CompanyToVacancyEntity[] {
    return this._companyToVacancy;
  }

  public addCompanyToVacancy(companyToVacancy: CompanyToVacancyEntity) {
    this.companyToVacancy.push(companyToVacancy);

    return this;
  }

  public getCompanyIdByVacancyId(vacancyId: TVacancyId) {
    return this.companyToVacancy.find(
      (companyToVacancy) => companyToVacancy.vacancyId === vacancyId,
    ).companyId;
  }

  public getAllVacanciesIdByCompanyId(companyId: TCompanyId) {
    return this.companyToVacancy
      .filter((companyToVacancy) => companyToVacancy.companyId === companyId)
      .map((companyToVacancy) => companyToVacancy.vacancyId);
  }
}
