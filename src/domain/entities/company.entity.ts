import { CompanyToVacancyWindowEntity } from './company-to-vacancy-window.entity';
import { TVacancyId, VacancyEntity } from './vacancy.entity';

export type TCompanyId = number;

export class CompanyEntity {
  constructor(
    private readonly _id: TCompanyId,
    private readonly _name: string,
    private readonly _vacancies?: VacancyEntity[],
    private readonly _companyToVacancyWindow?: CompanyToVacancyWindowEntity,
  ) {}

  get id(): TCompanyId {
    return this._id;
  }

  get name(): string {
    return this._name;
  }

  get vacancies(): VacancyEntity[] {
    return this._vacancies;
  }

  public getVacanciesId(): TVacancyId[] {
    return this._companyToVacancyWindow.getAllVacanciesIdByCompanyId(this.id);
  }

  static of(company: CompanyEntity) {
    const { id, name } = company;
    const entity = new CompanyEntity(id, name);
    return {
      id: company.id,
      name: entity.name,
    };
  }
}
