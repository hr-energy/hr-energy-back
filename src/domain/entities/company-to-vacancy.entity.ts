import { TCompanyId } from './company.entity';
import { TVacancyId } from './vacancy.entity';

export class CompanyToVacancyEntity {
  constructor(
    private readonly _companyId: TCompanyId,
    private readonly _vacancyId: TVacancyId,
  ) {}

  get companyId(): TCompanyId {
    return this._companyId;
  }

  get vacancyId(): TVacancyId {
    return this._vacancyId;
  }
}
