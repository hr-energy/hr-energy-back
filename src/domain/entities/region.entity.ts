export type TRegionId = number;

export class RegionEntity {
  constructor(private readonly _id: TRegionId) {}

  get id() {
    return this._id;
  }
}
