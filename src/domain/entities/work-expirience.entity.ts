export enum WorkExpirienceValue {
  Without = 'without',
  Low = 'low',
  Middle = 'middle',
  High = 'high',
}

export class WorkExpirience {
  constructor(private readonly _workExpirienceValue: WorkExpirienceValue) {}

  get workExpirienceValue() {
    return this._workExpirienceValue;
  }

  get workExpirienceUI() {
    switch (this._workExpirienceValue) {
      case WorkExpirienceValue.Without:
        return 'Без опыта';
      case WorkExpirienceValue.Low:
        return 'от 1 года до 3 лет';
      case WorkExpirienceValue.Middle:
        return 'от 3 до 6 лет';
      case WorkExpirienceValue.High:
        return 'Более 6 лет';
    }
  }
}
