export enum WorkScheduleValue {
  FullTime = 'full-time',
  PartTime = 'part-time',
  ProjectWork = 'project-work',
  Volunteering = 'volunteering',
  Internship = 'internship',
}

export class WorkSchedule {
  constructor(private readonly _workScheduleValue: WorkScheduleValue) {}

  get workSchedule(): WorkScheduleValue {
    return this._workScheduleValue;
  }

  get workScheduleUI() {
    switch (this._workScheduleValue) {
      case WorkScheduleValue.FullTime:
        return 'Полный день';
      case WorkScheduleValue.PartTime:
        return 'Частичная занятость';
      case WorkScheduleValue.ProjectWork:
        return 'Проектная работа';
      case WorkScheduleValue.Volunteering:
        return 'Волонтёрство';
      case WorkScheduleValue.Internship:
        return 'Стажировка';
    }
  }
}
