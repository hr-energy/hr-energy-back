import { CreateCompanyVacancyUseCase } from '../ports/in/create-company-vacancy.use-case';
import { CreateCompanyVacancyCommand } from '../ports/in/create-company-vacancy.command';
import { CreateCompanyVacancyPort } from '../ports/out/create-company-vacancy.port';
import { LoadCompanyPort } from '../ports/out/load-company.port';
import { LoadVacancyPort } from '../ports/out/load-vacancy.port';

export class CreateCompanyVacancyService
  implements CreateCompanyVacancyUseCase
{
  constructor(
    private readonly _loadCompanyPort: LoadCompanyPort,
    private readonly _loadVacancyPort: LoadVacancyPort,
    private readonly _createCompanyVacancyPort: CreateCompanyVacancyPort,
  ) {}
  async createCompanyVacancy(command: CreateCompanyVacancyCommand) {
    const company = await this._loadCompanyPort.loadCompany(command.companyId);
    const vacancy = await this._loadVacancyPort.loadVacancy(command.vacancyId);

    await this._createCompanyVacancyPort.createCompanyVacancyPort(
      company,
      vacancy,
    );

    return true;
  }
}
