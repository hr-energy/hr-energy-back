import { CompanyEntity, TCompanyId } from '../../entities/company.entity';
import { TVacancyId, VacancyEntity } from '../../entities/vacancy.entity';
import { CreateCompanyVacancyCommand } from '../../ports/in/create-company-vacancy.command';
import { CreateCompanyVacancyPort } from '../../ports/out/create-company-vacancy.port';
import { LoadCompanyPort } from '../../ports/out/load-company.port';
import { LoadVacancyPort } from '../../ports/out/load-vacancy.port';
import { instance, mock, when } from 'ts-mockito';
import { CreateCompanyVacancyService } from '../create-company-vacancy.service';

describe('CreateCompanyVacancy', () => {
  const VACANCY_ID = 100;
  const COMPANY_ID = 101;
  it('schould create vacancy by company id success', async () => {
    const loadCompanyPort = mock<LoadCompanyPort>();
    const loadVacancyPort = mock<LoadVacancyPort>();
    const createCompanyVacancyPort = mock<CreateCompanyVacancyPort>();

    function givenCompanyWithId(id: TCompanyId) {
      const mockedCompanyEntity = mock(CompanyEntity);
      when(mockedCompanyEntity.id).thenReturn(id);
      const company = instance(mockedCompanyEntity);
      when(loadCompanyPort.loadCompany(id)).thenReturn(company);

      return [company, mockedCompanyEntity];
    }

    function givenVacancyWithId(id: TVacancyId) {
      const mockedVacancyEntity = mock(VacancyEntity);
      when(mockedVacancyEntity.id).thenReturn(id);
      const vacancy = instance(mockedVacancyEntity);
      when(loadVacancyPort.loadVacancy(id)).thenReturn(vacancy);

      return [vacancy, mockedVacancyEntity];
    }

    const [firstCompany, mockedFirstCompanyEntity] =
      givenCompanyWithId(COMPANY_ID);

    const [firstVacancy, mockedFirstVacancyEntity] =
      givenVacancyWithId(VACANCY_ID);

    when(mockedFirstCompanyEntity.getVacanciesId()).thenReturn([]);
    when(mockedFirstVacancyEntity.getCompanyId()).thenReturn(null);

    const command = new CreateCompanyVacancyCommand(firstCompany.id, firstVacancy.id);
    const service = new CreateCompanyVacancyService(
      loadCompanyPort,
      loadVacancyPort,
      createCompanyVacancyPort,
    );

    const result = service.createCompanyVacancy(command);
    expect(result).toBeTruthy();
  });
});
