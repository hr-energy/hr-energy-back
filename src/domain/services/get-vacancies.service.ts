import { CompanyToVacancyEntity } from '../entities/company-to-vacancy.entity';
import { VacancyEntity } from '../entities/vacancy.entity';
import { GetVacanciesQuery } from '../ports/in/get-vacancies.query';
import { GetCompanyByIdPort } from '../ports/out/get-company-by-id.port';
import { GetCompanyVacancyByVacancyIdPort } from '../ports/out/get-company-vacany-by-vacancy-id.port';
import { GetVacanciesPort } from '../ports/out/get-vacancies.port';

export class GetVacanciesService implements GetVacanciesQuery {
  constructor(
    private readonly _getVacanciesPort: GetVacanciesPort,
    private readonly _getCompanyByIdPort: GetCompanyByIdPort,
    private readonly _getCompanyVacancyByVacancyIdPort: GetCompanyVacancyByVacancyIdPort,
  ) {}

  async getVacancies(): Promise<VacancyEntity[]> {
    const vacancyList = await this._getVacanciesPort.getVacancies();
    const vacancies = await vacancyList.map(async (vacancy) => {
      const companyVacancy: CompanyToVacancyEntity =
        await this._getCompanyVacancyByVacancyIdPort.getCompanyVacancyByVacancyId(
          vacancy.id,
        );
      const company = await this._getCompanyByIdPort.getCompanyById(
        companyVacancy.companyId,
      );
      const vacancyWithCompany = new VacancyEntity(
        vacancy.id,
        vacancy.name,
        vacancy.priceFrom,
        vacancy.priceTo,
        vacancy.regionId,
        vacancy.profAreaId,
        vacancy.workExpirience,
        vacancy.workSchedule,
        vacancy.timestamp,
        vacancy.description,
        company,
      );
      return vacancyWithCompany;
    });

    return Promise.all(vacancies);
  }
}
