import { CompanyEntity, TCompanyId } from '../entities/company.entity';
import { GetCompanyByIdQuery } from '../ports/in/get-company-by-id.query';
import { GetCompanyByIdPort } from '../ports/out/get-company-by-id.port';
import { GetCompanyVacancyByCompanyIdPort } from '../ports/out/get-company-vacancy-by-company-id.port';
import { GetVacanciesByIdsPort } from '../ports/out/get-vacancies-by-ids.port';

export class GetCompanyByIdService implements GetCompanyByIdQuery {
  constructor(
    private readonly _getCompanyByIdPort: GetCompanyByIdPort,
    private readonly _getCompanyVacancyByVacancyIdPort: GetCompanyVacancyByCompanyIdPort,
    private readonly _getVacanciesByIds: GetVacanciesByIdsPort,
  ) {}
  async getCompanyById(companyId: TCompanyId) {
    const company = await this._getCompanyByIdPort.getCompanyById(companyId);
    const companyVacancy =
      await this._getCompanyVacancyByVacancyIdPort.getCompanyVacancyByCompanyId(
        companyId,
      );
    const vacancies = await this._getVacanciesByIds.getVacanciesByIds(
      companyVacancy.map((cv) => cv.vacancyId),
    );

    const entity = new CompanyEntity(company.id, company.name, vacancies);

    return entity;
  }
}
