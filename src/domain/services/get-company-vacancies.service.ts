import { TCompanyId } from '../entities/company.entity';
import { GetCompanyVacanciesQuery } from '../ports/in/get-company-vacancies.query';
import { LoadCompanyPort } from '../ports/out/load-company.port';

export class GetCompanyVacanciesService implements GetCompanyVacanciesQuery {
  constructor(private readonly _loadCompanyPort: LoadCompanyPort) {}

  async getCompanyVacancies(companyId: TCompanyId) {
    return await (
      await this._loadCompanyPort.loadCompany(companyId)
    ).getVacanciesId();
  }
}
