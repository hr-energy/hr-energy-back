import { CreateVacancyCommand } from '../ports/in/create-vacancy.command';
import { CreateVacancyUseCase } from '../ports/in/create-vacancy.use-case';
import { CreateVacancyPort } from '../ports/out/create-vacancy.port';

export class CreateVacancyService implements CreateVacancyUseCase {
  constructor(private readonly _createVacancyPort: CreateVacancyPort) {}
  async createVacancy(command: CreateVacancyCommand) {
    const {
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      description,
    } = command;
    const vacancy = await this._createVacancyPort.createVacancy(
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      description,
    );

    return vacancy;
  }
}
