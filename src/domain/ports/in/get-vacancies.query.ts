import { VacancyEntity } from 'src/domain/entities/vacancy.entity';

export const GetVacanciesQuerySymbol = Symbol('GetVacanciesQuery');

export interface GetVacanciesQuery {
  getVacancies(): Promise<VacancyEntity[]>;
}
