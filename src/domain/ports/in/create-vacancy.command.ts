import { TProfAreaId } from 'src/domain/entities/prof-area.entity';
import { TRegionId } from 'src/domain/entities/region.entity';
import { WorkExpirienceValue } from 'src/domain/entities/work-expirience.entity';
import { WorkScheduleValue } from 'src/domain/entities/work-schedule.entity';

export class CreateVacancyCommand {
  constructor(
    private readonly _name: string,
    private readonly _priceFrom: number,
    private readonly _priceTo: number,
    private readonly _regionId: TRegionId,
    private readonly _profAreaId: TProfAreaId,
    private readonly _workExpirience: WorkExpirienceValue,
    private readonly _workSchedule: WorkScheduleValue,
    private readonly _description: string,
  ) {}

  get name(): string {
    return this._name;
  }

  public get workSchedule(): WorkScheduleValue {
    return this._workSchedule;
  }
  public get workExpirience(): WorkExpirienceValue {
    return this._workExpirience;
  }
  public get profAreaId(): TProfAreaId {
    return this._profAreaId;
  }
  public get regionId(): TRegionId {
    return this._regionId;
  }
  public get priceTo(): number {
    return this._priceTo;
  }
  public get priceFrom(): number {
    return this._priceFrom;
  }

  public get description(): string {
    return this._description;
  }
}
