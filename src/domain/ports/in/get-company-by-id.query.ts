import { CompanyEntity, TCompanyId } from 'src/domain/entities/company.entity';

export const GetCompanyByIdQuerySymbol = Symbol('GetCompanyByIdQuery');

export interface GetCompanyByIdQuery {
  getCompanyById(companyId: TCompanyId): Promise<CompanyEntity>;
}
