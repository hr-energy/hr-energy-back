import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';

export interface GetCompanyVacanciesQuery {
  getCompanyVacancies(companyId: TCompanyId): Promise<TVacancyId[]>;
}
