import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';

export class CreateCompanyVacancyCommand {
  constructor(
    private readonly _companyId: TCompanyId,
    private readonly _vacancyId: TVacancyId,
  ) {}

  get companyId(): TCompanyId {
    return this._companyId;
  }

  get vacancyId(): TVacancyId {
    return this._vacancyId;
  }
}
