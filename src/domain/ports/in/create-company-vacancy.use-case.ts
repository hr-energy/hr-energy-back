import { CreateCompanyVacancyCommand } from './create-company-vacancy.command';

export const CreateCompanyVacancyUseCaseSymbol = Symbol(
  'CreateCompanyVacancyUseCase',
);

export interface CreateCompanyVacancyUseCase {
  createCompanyVacancy(command: CreateCompanyVacancyCommand): Promise<boolean>;
}
