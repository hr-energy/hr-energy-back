import { VacancyEntity } from 'src/domain/entities/vacancy.entity';
import { CreateVacancyCommand } from './create-vacancy.command';

export const CreateVacancyUseCaseSymbol = Symbol('CreateVacancyUseCase');

export interface CreateVacancyUseCase {
  createVacancy(command: CreateVacancyCommand): Promise<VacancyEntity>;
}
