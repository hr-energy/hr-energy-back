export interface CreateCompanyVacancyPort {
  createCompanyVacancyPort<T, C>(company: T, vacancy: C): Promise<boolean>;
}
