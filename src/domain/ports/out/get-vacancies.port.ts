import { VacancyEntity } from 'src/domain/entities/vacancy.entity';

export interface GetVacanciesPort {
  getVacancies(): Promise<VacancyEntity[]>;
}
