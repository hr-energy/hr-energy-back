import { TProfAreaId } from 'src/domain/entities/prof-area.entity';
import { TRegionId } from 'src/domain/entities/region.entity';
import { VacancyEntity } from 'src/domain/entities/vacancy.entity';
import { WorkExpirienceValue } from 'src/domain/entities/work-expirience.entity';
import { WorkScheduleValue } from 'src/domain/entities/work-schedule.entity';

export interface CreateVacancyPort {
  createVacancy(
    name: string,
    priceFrom: number,
    priceTo: number,
    regionId: TRegionId,
    profAreaId: TProfAreaId,
    workExpirience: WorkExpirienceValue,
    workSchedule: WorkScheduleValue,
    description: string,
  ): Promise<VacancyEntity>;
}
