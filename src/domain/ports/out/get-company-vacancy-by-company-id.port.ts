import { CompanyToVacancyEntity } from 'src/domain/entities/company-to-vacancy.entity';
import { TCompanyId } from 'src/domain/entities/company.entity';

export interface GetCompanyVacancyByCompanyIdPort {
  getCompanyVacancyByCompanyId(
    companyId: TCompanyId,
  ): Promise<CompanyToVacancyEntity[]>;
}
