import { CompanyEntity, TCompanyId } from 'src/domain/entities/company.entity';

export interface GetCompanyByIdPort {
  getCompanyById(companyId: TCompanyId): Promise<CompanyEntity>;
}
