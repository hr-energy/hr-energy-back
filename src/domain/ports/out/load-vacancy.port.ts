import { TVacancyId, VacancyEntity } from 'src/domain/entities/vacancy.entity';

export interface LoadVacancyPort {
  loadVacancy(vacancyId: TVacancyId): Promise<VacancyEntity>;
}
