import { CompanyEntity, TCompanyId } from 'src/domain/entities/company.entity';

export interface LoadCompanyPort {
  loadCompany(companyId: TCompanyId): Promise<CompanyEntity>;
}
