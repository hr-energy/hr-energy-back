import { TVacancyId, VacancyEntity } from 'src/domain/entities/vacancy.entity';

export interface GetVacanciesByIdsPort {
  getVacanciesByIds(ids: TVacancyId[]): Promise<VacancyEntity[]>;
}
