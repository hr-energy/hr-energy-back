import { CompanyToVacancyEntity } from 'src/domain/entities/company-to-vacancy.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';

export interface GetCompanyVacancyByVacancyIdPort {
  getCompanyVacancyByVacancyId(
    vacancyId: TVacancyId,
  ): Promise<CompanyToVacancyEntity>;
}
