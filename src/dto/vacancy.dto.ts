import { TCompanyId } from 'src/domain/entities/company.entity';
import { TProfAreaId } from 'src/domain/entities/prof-area.entity';
import { TRegionId } from 'src/domain/entities/region.entity';
import { WorkExpirienceValue } from 'src/domain/entities/work-expirience.entity';
import { WorkScheduleValue } from 'src/domain/entities/work-schedule.entity';

export interface VacancyDTO {
  name: string;
  priceFrom: number;
  priceTo: number;
  regionId: TRegionId;
  profAreaId: TProfAreaId;
  workExpirience: WorkExpirienceValue;
  workSchedule: WorkScheduleValue;
  description: string;
  companyId: TCompanyId;
}
