import { Module } from '@nestjs/common';
import { CompanyWebController } from './company-web.controller';

@Module({
  controllers: [CompanyWebController],
})
export class CompanyWebModule {}
