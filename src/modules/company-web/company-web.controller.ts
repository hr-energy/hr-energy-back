import { Controller, Get, Inject, Param } from '@nestjs/common';
import { TCompanyId } from 'src/domain/entities/company.entity';
import {
  GetCompanyByIdQuery,
  GetCompanyByIdQuerySymbol,
} from 'src/domain/ports/in/get-company-by-id.query';

@Controller('/')
export class CompanyWebController {
  constructor(
    @Inject(GetCompanyByIdQuerySymbol)
    private readonly _getCompanyByIdQuery: GetCompanyByIdQuery,
  ) {}

  @Get('/company/:id')
  async getCompanyById(@Param('id') companyId: TCompanyId) {
    return await this._getCompanyByIdQuery.getCompanyById(companyId);
  }
}
