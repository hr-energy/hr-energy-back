import { TVacancyId } from 'src/domain/entities/vacancy.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('company', {})
export class CompanyOrmEntity {
  @PrimaryGeneratedColumn()
  id: TVacancyId;
  @Column()
  name: string;
}
