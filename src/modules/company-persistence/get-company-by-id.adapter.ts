import { InjectRepository } from '@nestjs/typeorm';
import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';
import { GetCompanyByIdPort } from 'src/domain/ports/out/get-company-by-id.port';
import { GetCompanyVacancyByCompanyIdPort } from 'src/domain/ports/out/get-company-vacancy-by-company-id.port';
import { GetVacanciesByIdsPort } from 'src/domain/ports/out/get-vacancies-by-ids.port';
import { In, Repository } from 'typeorm';
import { CompanyVacancyOrmEntity } from '../vacancy-persistence/company-vacancy.orm-entity';
import { VacancyMapper } from '../vacancy-persistence/vacancy.mapper';
import { VacancyOrmEntity } from '../vacancy-persistence/vacancy.orm-entity';
import { CompanyMapper } from './company.mapper';
import { CompanyOrmEntity } from './company.orm-entity';

export class GetCompanyByIdAdapter
  implements
    GetCompanyByIdPort,
    GetCompanyVacancyByCompanyIdPort,
    GetVacanciesByIdsPort
{
  constructor(
    @InjectRepository(CompanyOrmEntity)
    private readonly _companyRepository: Repository<CompanyOrmEntity>,
    @InjectRepository(CompanyVacancyOrmEntity)
    private readonly _companyVacancyRepository: Repository<CompanyVacancyOrmEntity>,
    @InjectRepository(VacancyOrmEntity)
    private readonly _vacancyRepository: Repository<VacancyOrmEntity>,
  ) {}

  async getCompanyById(companyId: TCompanyId) {
    const company = await this._companyRepository.findOne({ id: companyId });
    if (company === undefined) {
      throw new Error('Cant find company');
    }
    return CompanyMapper.mapToDomain(company);
  }
  async getCompanyVacancyByCompanyId(companyId: TCompanyId) {
    const companyVacancy = await this._companyVacancyRepository.find({
      company_id: companyId,
    });

    console.log(companyVacancy);

    if (companyVacancy === undefined) {
      throw new Error('Cant find companyVacancy');
    }

    return companyVacancy.map((cv) =>
      VacancyMapper.companyVacancyMapDomain(cv),
    );
  }
  async getVacanciesByIds(ids: TVacancyId[]) {
    const vacancies = await this._vacancyRepository.find({
      id: In(ids),
    });

    console.log(ids, vacancies);

    return vacancies.map((vacancy) => VacancyMapper.mapToDomain(vacancy));
  }
}
