import { CompanyEntity } from 'src/domain/entities/company.entity';
import { CompanyOrmEntity } from './company.orm-entity';

export class CompanyMapper {
  static mapToDomain(company: CompanyOrmEntity): CompanyEntity {
    const { id, name } = company;
    return new CompanyEntity(id, name);
  }
}
