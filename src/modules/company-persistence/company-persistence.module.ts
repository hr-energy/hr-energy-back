import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { GetCompanyByIdQuerySymbol } from 'src/domain/ports/in/get-company-by-id.query';
import { GetCompanyByIdService } from 'src/domain/services/get-company-by-id.service';
import { CompanyVacancyOrmEntity } from '../vacancy-persistence/company-vacancy.orm-entity';
import { VacancyOrmEntity } from '../vacancy-persistence/vacancy.orm-entity';
import { CompanyOrmEntity } from './company.orm-entity';
import { GetCompanyByIdAdapter } from './get-company-by-id.adapter';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([
      VacancyOrmEntity,
      CompanyOrmEntity,
      CompanyVacancyOrmEntity,
    ]),
  ],
  providers: [
    GetCompanyByIdAdapter,
    {
      provide: GetCompanyByIdQuerySymbol,
      useFactory: (adapter) => {
        return new GetCompanyByIdService(adapter, adapter, adapter);
      },
      inject: [GetCompanyByIdAdapter],
    },
  ],
  exports: [GetCompanyByIdQuerySymbol],
})
export class CompanyPersistenceModule {}
