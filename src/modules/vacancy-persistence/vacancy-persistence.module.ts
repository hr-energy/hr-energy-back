import { Global, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { CreateCompanyVacancyUseCaseSymbol } from 'src/domain/ports/in/create-company-vacancy.use-case';
import { CreateVacancyUseCaseSymbol } from 'src/domain/ports/in/create-vacancy.use-case';
import { GetVacanciesQuerySymbol } from 'src/domain/ports/in/get-vacancies.query';
import { CreateCompanyVacancyService } from 'src/domain/services/create-company-vacancy.service';
import { CreateVacancyService } from 'src/domain/services/create-vacancy.service';
import { GetVacanciesService } from 'src/domain/services/get-vacancies.service';
import { CompanyOrmEntity } from '../company-persistence/company.orm-entity';
import { CompanyVacancyAdapter } from './company-vacancy.adapter';
import { CompanyVacancyOrmEntity } from './company-vacancy.orm-entity';
import { VacancyAdapter } from './create-vacancy.adapter';
import { GetVacanciesAdapter } from './get-vacancies.adapter';
import { VacancyOrmEntity } from './vacancy.orm-entity';

@Global()
@Module({
  imports: [
    TypeOrmModule.forFeature([
      VacancyOrmEntity,
      CompanyOrmEntity,
      CompanyVacancyOrmEntity,
    ]),
  ],
  providers: [
    CompanyVacancyAdapter,
    VacancyAdapter,
    GetVacanciesAdapter,
    {
      provide: CreateVacancyUseCaseSymbol,
      useFactory: (adapter) => {
        return new CreateVacancyService(adapter);
      },
      inject: [VacancyAdapter],
    },
    {
      provide: CreateCompanyVacancyUseCaseSymbol,
      useFactory: (adapter) => {
        return new CreateCompanyVacancyService(adapter, adapter, adapter);
      },
      inject: [CompanyVacancyAdapter],
    },
    {
      provide: GetVacanciesQuerySymbol,
      useFactory: (query) => {
        return new GetVacanciesService(query, query, query);
      },
      inject: [GetVacanciesAdapter],
    },
  ],
  exports: [
    CreateCompanyVacancyUseCaseSymbol,
    CreateVacancyUseCaseSymbol,
    GetVacanciesQuerySymbol,
  ],
})
export class VacancyPersistenceModule {}
