import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';
import { LoadCompanyPort } from 'src/domain/ports/out/load-company.port';
import { LoadVacancyPort } from 'src/domain/ports/out/load-vacancy.port';
import { CompanyOrmEntity } from '../company-persistence/company.orm-entity';
import { VacancyOrmEntity } from '../vacancy-persistence/vacancy.orm-entity';
import { CompanyMapper } from '../company-persistence/company.mapper';
import { VacancyMapper } from '../vacancy-persistence/vacancy.mapper';
import { CompanyVacancyOrmEntity } from './company-vacancy.orm-entity';
import { CreateCompanyVacancyPort } from 'src/domain/ports/out/create-company-vacancy.port';

@Injectable()
export class CompanyVacancyAdapter
  implements LoadCompanyPort, LoadVacancyPort, CreateCompanyVacancyPort
{
  constructor(
    @InjectRepository(VacancyOrmEntity)
    private readonly _vacancyRepository: Repository<VacancyOrmEntity>,
    @InjectRepository(CompanyOrmEntity)
    private readonly _companyRepository: Repository<CompanyOrmEntity>,
    @InjectRepository(CompanyVacancyOrmEntity)
    private readonly _companyVacancyRepository: Repository<CompanyVacancyOrmEntity>,
  ) {}
  async loadCompany(companyId: TCompanyId) {
    const company = await this._companyRepository.findOne({
      id: companyId,
    });

    if (company === undefined) {
      throw new Error('Cant find company');
    }

    return CompanyMapper.mapToDomain(company);
  }
  async loadVacancy(vacancyId: TVacancyId) {
    const vacancy = await this._vacancyRepository.findOne({ id: vacancyId });

    if (vacancy === undefined) {
      throw new Error('Cant find vacancy');
    }

    return VacancyMapper.mapToDomain(vacancy);
  }

  async createCompanyVacancyPort<CompanyOrmEntity, VacancyOrmEntity>(
    company: CompanyOrmEntity,
    vacancy: VacancyOrmEntity,
  ) {
    await this._companyVacancyRepository.insert({
      company_id: company['id'],
      vacancy_id: vacancy['id'],
    });

    return true;
  }
}
