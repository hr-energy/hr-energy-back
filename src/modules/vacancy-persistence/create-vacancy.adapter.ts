import { InjectRepository } from '@nestjs/typeorm';
import { TProfAreaId } from 'src/domain/entities/prof-area.entity';
import { TRegionId } from 'src/domain/entities/region.entity';
import { WorkExpirienceValue } from 'src/domain/entities/work-expirience.entity';
import { WorkScheduleValue } from 'src/domain/entities/work-schedule.entity';
import { CreateVacancyPort } from 'src/domain/ports/out/create-vacancy.port';
import { Repository } from 'typeorm';
import { VacancyMapper } from './vacancy.mapper';
import { VacancyOrmEntity } from './vacancy.orm-entity';

export class VacancyAdapter implements CreateVacancyPort {
  constructor(
    @InjectRepository(VacancyOrmEntity)
    private readonly _vacancyRepository: Repository<VacancyOrmEntity>,
  ) {}
  async createVacancy(
    name: string,
    priceFrom: number,
    priceTo: number,
    regionId: TRegionId,
    profAreaId: TProfAreaId,
    workExpirience: WorkExpirienceValue,
    workSchedule: WorkScheduleValue,
    description: string,
  ) {
    const vacancy = await this._vacancyRepository.insert({
      name,
      price_from: priceFrom,
      price_to: priceTo,
      region_id: regionId,
      prof_area_id: profAreaId,
      work_expirience: workExpirience,
      work_schedule: workSchedule,
      description,
      timestamp: new Date().toString(),
    });
    return VacancyMapper.mapToDomain(
      vacancy.identifiers[0] as VacancyOrmEntity,
    );
  }
}
