import { CompanyToVacancyEntity } from 'src/domain/entities/company-to-vacancy.entity';
import { VacancyEntity } from 'src/domain/entities/vacancy.entity';
import { CompanyVacancyOrmEntity } from './company-vacancy.orm-entity';
import { VacancyOrmEntity } from './vacancy.orm-entity';
export class VacancyMapper {
  static mapToDomain(vacancy: VacancyOrmEntity): VacancyEntity {
    const {
      id,
      name,
      price_from,
      price_to,
      region_id,
      prof_area_id,
      work_expirience,
      work_schedule,
      description,
      timestamp,
    } = vacancy;
    return new VacancyEntity(
      id,
      name,
      price_from,
      price_to,
      region_id,
      prof_area_id,
      work_expirience,
      work_schedule,
      description,
      timestamp,
    );
  }
  static companyVacancyMapDomain(
    compainVacancy: CompanyVacancyOrmEntity,
  ): CompanyToVacancyEntity {
    const { company_id, vacancy_id } = compainVacancy;
    return new CompanyToVacancyEntity(company_id, vacancy_id);
  }

  static mapToCompanyVacancyOrmEntity(
    compainVacancy: CompanyToVacancyEntity,
  ): CompanyVacancyOrmEntity {
    const companyVacancyOrm = new CompanyVacancyOrmEntity();
    companyVacancyOrm.company_id = compainVacancy.companyId;
    companyVacancyOrm.vacancy_id = compainVacancy.vacancyId;

    return companyVacancyOrm;
  }
}
