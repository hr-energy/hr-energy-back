import { TProfAreaId } from 'src/domain/entities/prof-area.entity';
import { TRegionId } from 'src/domain/entities/region.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';
import { WorkExpirienceValue } from 'src/domain/entities/work-expirience.entity';
import { WorkScheduleValue } from 'src/domain/entities/work-schedule.entity';
import { Column, Entity, PrimaryGeneratedColumn } from 'typeorm';

@Entity('vacancy', {})
export class VacancyOrmEntity {
  @PrimaryGeneratedColumn()
  id: TVacancyId;
  @Column()
  name: string;
  @Column()
  price_from: number;
  @Column()
  price_to: number;
  @Column()
  region_id: TRegionId;
  @Column()
  prof_area_id: TProfAreaId;
  @Column()
  work_expirience: WorkExpirienceValue;
  @Column()
  work_schedule: WorkScheduleValue;
  @Column()
  description: string;
  @Column()
  timestamp: string;
}
