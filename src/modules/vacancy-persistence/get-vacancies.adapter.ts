import { InjectRepository } from '@nestjs/typeorm';
import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId, VacancyEntity } from 'src/domain/entities/vacancy.entity';
import { GetCompanyByIdPort } from 'src/domain/ports/out/get-company-by-id.port';
import { GetCompanyVacancyByVacancyIdPort } from 'src/domain/ports/out/get-company-vacany-by-vacancy-id.port';
import { GetVacanciesPort } from 'src/domain/ports/out/get-vacancies.port';
import { Repository } from 'typeorm';
import { CompanyMapper } from '../company-persistence/company.mapper';
import { CompanyOrmEntity } from '../company-persistence/company.orm-entity';
import { CompanyVacancyOrmEntity } from './company-vacancy.orm-entity';
import { VacancyMapper } from './vacancy.mapper';
import { VacancyOrmEntity } from './vacancy.orm-entity';

export class GetVacanciesAdapter
  implements
    GetVacanciesPort,
    GetCompanyByIdPort,
    GetCompanyVacancyByVacancyIdPort
{
  constructor(
    @InjectRepository(VacancyOrmEntity)
    private readonly _vacancyRepository: Repository<VacancyOrmEntity>,
    @InjectRepository(CompanyVacancyOrmEntity)
    private readonly _companyVacancyRepository: Repository<CompanyVacancyOrmEntity>,
    @InjectRepository(CompanyOrmEntity)
    private readonly _companyRepository: Repository<CompanyOrmEntity>,
  ) {}
  async getVacancies(): Promise<VacancyEntity[]> {
    const vacancies = await this._vacancyRepository.find();
    return vacancies.map((vacancy) => VacancyMapper.mapToDomain(vacancy));
  }

  async getCompanyVacancyByVacancyId(vacancyId: TVacancyId) {
    const companyVacancy = await this._companyVacancyRepository.findOne({
      vacancy_id: vacancyId,
    });

    if (companyVacancy === undefined) {
      throw new Error('Cant find companyVacancy');
    }

    return VacancyMapper.companyVacancyMapDomain(companyVacancy);
  }

  async getCompanyById(companyId: TCompanyId) {
    const company = await this._companyRepository.findOne({ id: companyId });
    return CompanyMapper.mapToDomain(company);
  }
}
