import { Entity, PrimaryGeneratedColumn, Column } from 'typeorm';
import { TCompanyId } from 'src/domain/entities/company.entity';
import { TVacancyId } from 'src/domain/entities/vacancy.entity';

@Entity('company_vacancy', {})
export class CompanyVacancyOrmEntity {
  @PrimaryGeneratedColumn()
  id: number;
  @Column()
  company_id: TCompanyId;
  @Column()
  vacancy_id: TVacancyId;
}
