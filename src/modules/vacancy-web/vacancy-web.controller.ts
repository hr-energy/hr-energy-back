import { Body, Controller, Post, Inject, Get } from '@nestjs/common';
import {
  CreateCompanyVacancyUseCase,
  CreateCompanyVacancyUseCaseSymbol,
} from 'src/domain/ports/in/create-company-vacancy.use-case';
import { CreateCompanyVacancyCommand } from 'src/domain/ports/in/create-company-vacancy.command';
import {
  CreateVacancyUseCase,
  CreateVacancyUseCaseSymbol,
} from 'src/domain/ports/in/create-vacancy.use-case';
import { CreateVacancyCommand } from 'src/domain/ports/in/create-vacancy.command';
import { VacancyDTO } from 'src/dto/vacancy.dto';
import {
  GetVacanciesQuery,
  GetVacanciesQuerySymbol,
} from 'src/domain/ports/in/get-vacancies.query';
import { VacancyEntity } from 'src/domain/entities/vacancy.entity';

@Controller('/')
export class VacancyWebContoller {
  constructor(
    @Inject(CreateCompanyVacancyUseCaseSymbol)
    private readonly _createCompanyVacancyUseCase: CreateCompanyVacancyUseCase,
    @Inject(CreateVacancyUseCaseSymbol)
    private readonly _createVacancyUseCase: CreateVacancyUseCase,
    @Inject(GetVacanciesQuerySymbol)
    private readonly _getVacancies: GetVacanciesQuery,
  ) {}

  @Post('/vacancy/create')
  async createVacancy(@Body() data: VacancyDTO) {
    const {
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      description,
      companyId,
    } = data;
    const commandCreateVacancy = new CreateVacancyCommand(
      name,
      priceFrom,
      priceTo,
      regionId,
      profAreaId,
      workExpirience,
      workSchedule,
      description,
    );
    const vacancy = await this._createVacancyUseCase.createVacancy(
      commandCreateVacancy,
    );
    const commandCreateCompanyVacancy = new CreateCompanyVacancyCommand(
      Number(companyId),
      vacancy.id,
    );
    const result = await this._createCompanyVacancyUseCase.createCompanyVacancy(
      commandCreateCompanyVacancy,
    );

    return { result };
  }

  @Get('/vacancies')
  async getVacancies() {
    const vacancies = await this._getVacancies.getVacancies();
    return vacancies.map((vacancy) => {
      const entity = VacancyEntity.of(vacancy);
      return entity;
    });
  }
}
