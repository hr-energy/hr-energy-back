import { Module } from '@nestjs/common';
import { VacancyWebContoller } from './vacancy-web.controller';

@Module({
  controllers: [VacancyWebContoller],
})
export class VacancyWebModule {}
