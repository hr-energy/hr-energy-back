import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { join } from 'path';
import { CompanyPersistenceModule } from './modules/company-persistence/company-persistence.module';
import { CompanyWebModule } from './modules/company-web/company-web.module';
import { VacancyPersistenceModule } from './modules/vacancy-persistence/vacancy-persistence.module';
import { VacancyWebModule } from './modules/vacancy-web/vacancy-web.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: join(__dirname, '..', 'data', 'data.db'),
      logging: true,
      autoLoadEntities: true,
    }),
    VacancyPersistenceModule,
    VacancyWebModule,
    CompanyPersistenceModule,
    CompanyWebModule,
  ],
})
export class AppModule {}
